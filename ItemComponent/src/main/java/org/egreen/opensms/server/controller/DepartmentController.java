package org.egreen.opensms.server.controller;

import org.apache.log4j.Logger;
import org.egreen.opensms.server.entity.Department;
import org.egreen.opensms.server.service.DepartmentDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Pramoda Fernando on 8/19/2014.
 */
@Controller
@RequestMapping("mintbooks/v1/department")
public class DepartmentController {

    private static final Logger LOGGER = Logger.getLogger(DepartmentController.class);

    @Autowired
    private DepartmentDAOService categoryDAOService;

    /**
     * Save Department
     *
     * @param category
     * @return
     */
    @RequestMapping(value = "save", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public ResponseMessage addDepartment(@RequestBody Department category) {
        LOGGER.info(category);
        Long res = categoryDAOService.saveDepartment(category);
        ResponseMessage responseMessage;
        if (res != null) {
            responseMessage = ResponseMessage.SUCCESS;
            responseMessage.setData(res);
        } else {
            responseMessage = ResponseMessage.DANGER;
            responseMessage.setData(res);
        }
        return responseMessage;
    }

    /**
     * Update Department
     *
     * @param category
     * @return
     */
    @RequestMapping(value = "update", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    public ResponseMessage updateDepartment(@RequestBody Department category) {
        LOGGER.info(category);
        boolean result = categoryDAOService.updateCatogory(category);
        return ResponseMessage.SUCCESS;
    }

    /**
     * Search Department By Name
     *
     * @param categoryName
     * @return
     */
    @RequestMapping(value = "search", method = RequestMethod.GET)
    @ResponseBody
    public ResponseMessage searchDepartmentByName(@RequestParam("categoryName") String categoryName) {
        List<Department> res = categoryDAOService.searchDepartmentByName(categoryName);

        ResponseMessage responseMessage;
        if (res != null) {
            responseMessage = ResponseMessage.SUCCESS;
            responseMessage.setData(res);
        } else {
            responseMessage = ResponseMessage.DANGER;
            responseMessage.setData(res);
        }
        return responseMessage;
    }

    /**
     * Search All Department
     *
     * @return
     */
    @RequestMapping(value = "search_all", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public ResponseMessage searchAllDepartment() {
        List<Department> res = categoryDAOService.searchAllDepartment();

        ResponseMessage responseMessage;
        if (res != null) {
            responseMessage = ResponseMessage.SUCCESS;
            responseMessage.setData(res);
        } else {
            responseMessage = ResponseMessage.DANGER;
            responseMessage.setData(res);
        }
        return responseMessage;
    }

    /**
     * Search Department By Id
     *
     * @return
     */
    @RequestMapping(value = "search_categoryById", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public List<Department> searchDepartmentById() {
        List<Department> list = categoryDAOService.searchAllDepartment();
        LOGGER.info(list);
        return list;
    }


    /**
     * Remove Department
     *
     * @param categoryId
     * @return
     */
    @RequestMapping(value = "removeDepartment", method = RequestMethod.GET)
    @ResponseBody
    public ResponseMessage searchDepartmentById(@RequestParam("categoryId") Long categoryId) {
        Integer res = categoryDAOService.deleteDepartment(categoryId);

        ResponseMessage responseMessage;
        if (res != null) {
            responseMessage = ResponseMessage.SUCCESS;
            responseMessage.setData(res);
        } else {
            responseMessage = ResponseMessage.DANGER;
            responseMessage.setData(res);
        }
        return responseMessage;
    }

    /**
     * Find Non Parent
     *
     * @return
     */
    @RequestMapping(value = "search_all_nonParent", method = RequestMethod.GET)
    @ResponseBody
    public List<Department> searchAllDepartmentNonParent() {
        List<Department> list = categoryDAOService.searchAllNonParent();
        LOGGER.info(list);
        return list;
    }

    /**
     *
     * Pagination Department By Range
     *
     * @param limit
     * @param offset
     * @return
     */
//    @RequestMapping(value = "paginateDepartmentByRange",method = RequestMethod.GET)
//    @ResponseBody
//    public  List<Department>  getAllDepartmentByRange(@RequestParam("limit")Integer limit,@RequestParam("offset")Integer offset ) {
//
//        List<Department> list  = categoryDAOService.getAllDepartmentByRange(limit, offset);
//        return list;
//    }


    /**
     * Get Row Count
     *
     * @return
     */
    @RequestMapping(value = "getRowCount", method = RequestMethod.GET)
    @ResponseBody
    public Long getRowCount() {

        Long a = categoryDAOService.getAllCount();
        return a;
    }

    @RequestMapping(value = "ob", method = RequestMethod.GET)
    @ResponseBody
    public Department ob() {
        return new Department();
    }
}
