package org.egreen.opensms.server.service;

import org.apache.log4j.Logger;
import org.egreen.opensms.server.dao.DepartmentDAOController;
import org.egreen.opensms.server.entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Pramoda Fernando on 8/19/2014.
 */
@Service
public class DepartmentDAOService {


    private static final Logger LOGGER = Logger.getLogger(DepartmentDAOService.class);


    private static final AtomicInteger COUNTER = new AtomicInteger();


    @Autowired
    private DepartmentDAOController daoController;


    /**
     * Save Department
     *
     * @param category
     * @return
     */
    public Long saveDepartment(Department category) {
        long categoryId = new Date().getTime();
        category.setDepartmentId(categoryId);
        Long res = daoController.create(category);
        return res;
    }

    /**
     *
     * Update Department
     *
     * @param category
     * @return
     */
    public  boolean updateCatogory(Department category){
        LOGGER.info(category);
        daoController.update(category);
        return true;
    }

    /**
     * Search Catogory By ID
     *
     * @param categoryname
     * @return
     */
    public List<Department> searchDepartmentByName(String categoryname) {
        List<Department> categoryList = daoController.findDepartmentByQuery(categoryname);
        return categoryList;
    }

    /***
     *
     * Get All Department
     *
     * @return
     */
    public List<Department> searchAllDepartment() {
        List<Department> categoryList = daoController.findAllDepartment();
        return categoryList;
    }


    /**
     *
     * Find Non Parent
     *
     * @return
     */
    public List<Department> searchAllNonParent() {
        List<Department> categoryList = daoController.findNonParent();
        return categoryList;
    }


    /**
     *
     * Delete Department
     *
     * @param categoryId
     * @return
     */
    public Integer deleteDepartment(Long categoryId) {
       // Department read = daoController.read(categoryId);
        int delete = daoController.deleteDepartment(categoryId);
       return delete;
    }

    /**
     *
     * Get All Department By Range
     *
     * @param limit
     * @param offset
     * @return
     */
//    public List<Department> getAllDepartmentByRange(Integer limit, Integer offset) {
//        return daoController.getAllOrder(limit,offset,"");
//    }

    /**
     *
     * Get All Count
     *
     * @return
     */
    public Long getAllCount() {
        return daoController.getAllCount();
    }

    public Department getDepartmentByDepartmentId(Long categoryId) {
        return daoController.read(categoryId);
    }


}
