package org.egreen.opensms.server.controller;

import org.egreen.opensms.server.entity.Category;
import org.egreen.opensms.server.service.CategoryDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by Pramoda Fernando on 12/19/2014.
 */


@Controller
@RequestMapping("mintbooks/v1/mobile/category")
public class MobileCategoryController {
//
//    @Autowired
//    private ItemDAOService itemDAOService;
//
//    @Autowired
//    private BatchDAOController batchDAOController;

    @Autowired
    private CategoryDAOService categoryDAOService;
//
//
    /**
     * Search All Category
     *
     * @return
     */
    @RequestMapping(value = "search_all", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public List<Category> searchAllCategory() {
        List<Category> res = categoryDAOService.searchAllCategory();
        return res;
    }
//
//
//    @RequestMapping(value = "searchItemByItemId", method = RequestMethod.GET)
//    @ResponseBody
//    public ItemStockModel searchItemByItemId(@RequestParam("itemId") String itemId) {
//        Item item = itemDAOService.searchItemByItemId(itemId);
//        List<ItemStockModel> itemStockModelsList = new ArrayList<ItemStockModel>();
//
//            ItemStockModel itemStockModel = new ItemStockModel();
//            Category category = itemDAOService.searchCategoryName(item.getCategory());
//            Unit unit = itemDAOService.searchUnitName(item.getUnit());
//            List<BatchEntity> batchByItemId = batchDAOController.findBatchByItemId(item.getItemId());
//            double quantity = 0;
//            for (BatchEntity batchEntity : batchByItemId) {
//                quantity += batchEntity.getFakequantity().doubleValue();
//            }
//            itemStockModel.setMaxQty(quantity);
//            itemStockModel.setItemId(item.getItemId());
//            itemStockModel.setName(item.getName());
//
//            itemStockModel.setBrandName(category.getCategory());
//            itemStockModel.setUnitName(unit.getUnit());
////            if (quantity != 0) {
////                itemStockModelsList.add(itemStockModel);
////            }
//
//        return itemStockModel;
//    }


}
