package org.egreen.opensms.server.controller;


import org.egreen.opensms.server.entity.Customer;
import org.egreen.opensms.server.service.CustomerDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by Pramoda Fernando on 12/18/2014.
 */

@Controller
@RequestMapping("mintbooks/v1/mobile/customer/")
public class MobileCustomerController {


    @Autowired
    private CustomerDAOService daoService;



    @RequestMapping(value = "search_all_customer",method = RequestMethod.GET)
    @ResponseBody
    public List<Customer> search_all_contactDetails(){

        List<Customer> res =  daoService.searchAllContactDetails();
              return res;
    }
//
//    @RequestMapping(value = "search", method = RequestMethod.GET)
//    @ResponseBody
//    public List<UserContactDetail> getAllCust(@RequestParam("userId") Integer userId,@RequestParam("custName") String custName) {
//        List<UserContactDetail> userModels = new ArrayList<UserContactDetail>();
//        if (userId != null && custName != null) {
//            List<UserContactDetail> customers = customerControllerService.findCustomers(custName);
//            for (UserContactDetail userContactDetail : customers) {
//                userModels.add(userContactDetail);
//            }
//        } else {
//            List<Customer> list = customerControllerService.getAllCustomer();
//            for (Customer customer : list) {
//                List<UserContactDetail> userContactDetails = userContactDetailsControllerService.getallCustomerByName(customer.getName());
//                for (UserContactDetail userContactDetail : userContactDetails) {
//                    userModels.add(userContactDetail);
//                }
//            }
//        }
//        return userModels;
//    }


}
