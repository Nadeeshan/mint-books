package org.egreen.opensms.server.controller;

import org.egreen.opensms.server.entity.Item;
import org.egreen.opensms.server.service.ItemDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by Pramoda Fernando on 12/19/2014.
 */


@Controller
@RequestMapping("mintbooks/v1/mobile/item/")
public class MobileItemController {

    @Autowired
    private ItemDAOService itemDAOService;
//
//    @Autowired
//    private BatchDAOController batchDAOController;
//
//
//    @RequestMapping(value = "searchItemByItemName", method = RequestMethod.GET)
//    @ResponseBody
//    public List<ItemStockModel> searchItemByItemName(@RequestParam("itemName") String itemName) {
//        List<Item> list = itemDAOService.searchItemByName(itemName);
//       List<ItemStockModel> itemStockModelsList = new ArrayList<ItemStockModel>();
//        for (Item item : list) {
//            ItemStockModel itemStockModel = new ItemStockModel();
//            Category category = itemDAOService.searchCategoryName(item.getCategory());
//            Unit unit = itemDAOService.searchUnitName(item.getUnit());
//            List<BatchEntity> batchByItemId = batchDAOController.findBatchByItemId(item.getItemId());
//            double quantity = 0;
//            for (BatchEntity batchEntity : batchByItemId) {
//                quantity += batchEntity.getFakequantity().doubleValue();
//            }
//            itemStockModel.setMaxQty(quantity);
//            itemStockModel.setItemId(item.getItemId());
//            itemStockModel.setName(item.getName());
//
//            itemStockModel.setBrandName(category.getCategory());
//            itemStockModel.setUnitName(unit.getUnit());
//            if (quantity != 0) {
//                itemStockModelsList.add(itemStockModel);
//            }
//        }
//        return itemStockModelsList;
//    }

    @RequestMapping(value = "getItemDetailsByCategoryId", method = RequestMethod.GET)
    @ResponseBody
    public ResponseMessage getItemDetailsByCategoryId(@RequestParam("categoryId")Long categoryId) {
        List<Item> res = itemDAOService.getItemDetailsByCategoryId(categoryId);
        ResponseMessage responseMessage;
        if (res != null) {
            responseMessage = ResponseMessage.SUCCESS;
            responseMessage.setData(res);
        } else {
            responseMessage = ResponseMessage.DANGER;
            responseMessage.setData(res);
        }
        return responseMessage;
    }


//
//    @RequestMapping(value = "searchItemByItemId", method = RequestMethod.GET)
//    @ResponseBody
//    public ItemStockModel searchItemByItemId(@RequestParam("itemId") String itemId) {
//        Item item = itemDAOService.searchItemByItemId(itemId);
//        List<ItemStockModel> itemStockModelsList = new ArrayList<ItemStockModel>();
//
//            ItemStockModel itemStockModel = new ItemStockModel();
//            Category category = itemDAOService.searchCategoryName(item.getCategory());
//            Unit unit = itemDAOService.searchUnitName(item.getUnit());
//            List<BatchEntity> batchByItemId = batchDAOController.findBatchByItemId(item.getItemId());
//            double quantity = 0;
//            for (BatchEntity batchEntity : batchByItemId) {
//                quantity += batchEntity.getFakequantity().doubleValue();
//            }
//            itemStockModel.setMaxQty(quantity);
//            itemStockModel.setItemId(item.getItemId());
//            itemStockModel.setName(item.getName());
//
//            itemStockModel.setBrandName(category.getCategory());
//            itemStockModel.setUnitName(unit.getUnit());
////            if (quantity != 0) {
////                itemStockModelsList.add(itemStockModel);
////            }
//
//        return itemStockModel;
//    }


}
