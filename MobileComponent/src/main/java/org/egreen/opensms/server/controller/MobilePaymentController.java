package org.egreen.opensms.server.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Pramoda Fernando on 12/22/2014.
 */
@Controller
@RequestMapping("mobile/payment")
public class MobilePaymentController {
//
//    @Autowired
//    private GsrPaymentDAOService gsrPaymentDAOService;
//
//    @Autowired
//    private PreOrderService preOrderService;
//
//    @Autowired
//    private GsrOrderDAOService gsrOrderDAOService;
//
//    @Autowired
//    private ChequeDetailsDAOService chequeDetailsDAOService;
//
//    @RequestMapping(value = "getAllGsrPaymentByEmployee", method = RequestMethod.GET)
//    @ResponseBody
//    public List<AllPaymentDetails> getAllGsrPaymentByEmployee(@RequestParam("employeeId") Integer employeeId) {
//        List<PreOrder> allOpenSimpleOrders = preOrderService.getAllOpenSimpleOrders(employeeId);
//        System.out.println("PreOrder"+allOpenSimpleOrders);
//        List<GsrOrder> allGsrOrdersByPreOrderId=null;
//        List<AllPaymentDetails> allGsrPaymentReport = null;
//        for (PreOrder allOpenSimpleOrder : allOpenSimpleOrders) {
//            allGsrOrdersByPreOrderId = gsrOrderDAOService.getAllGsrOrdersByPreOrderId(allOpenSimpleOrder.getPreOrderId());
//            System.out.println("GsrOrder PreOrderID :"+allGsrOrdersByPreOrderId);
//            for (GsrOrder gsrOrder : allGsrOrdersByPreOrderId) {
//                System.out.println("Gsr Order : "+gsrOrder.getGsrOrderId());
//                List<GsrPayment> gsrPayments = gsrPaymentDAOService.searchGsnPaymentByOrderId(gsrOrder.getGsrOrderId());
//                for (GsrPayment gsrPayment : gsrPayments) {
//                    allGsrPaymentReport = getAllGsrPaymentReport(gsrPayment.getGsrOrder());
//                    System.out.println("GsrPayment : "+allGsrPaymentReport);
//                }
//            }
//        }
//        return allGsrPaymentReport;
//    }
//
//
//
//
//    public List<AllPaymentDetails> getAllGsrPaymentReport(Long gsrOrder ) {
//        List<AllPaymentDetails> paymentlist = new ArrayList<AllPaymentDetails>();
//        List<GsrPayment> list = gsrPaymentDAOService.getAllPayments();
//        double cashAmount = 0;
//        AllPaymentDetails allPaymentDetails = new AllPaymentDetails();
//        List<ChequeDetails> details = new ArrayList<ChequeDetails>();
//        List<GsrPayment> lsGsr = new ArrayList<GsrPayment>();
//        double chequeAmount = 0;
//        long gsrOrderId = 0;
//        for (GsrPayment gsrPayment : list) {
//            lsGsr.add(gsrPayment);
//            cashAmount += gsrPayment.getAmount().doubleValue();
//            allPaymentDetails.setGsrCashPayments(lsGsr);
//            allPaymentDetails.setFull_cash_amount(cashAmount);
//            gsrOrderId = gsrPayment.getGsrOrder();
//        }
//        List<ChequeDetails> chequeDetailses = chequeDetailsDAOService.searchChequeDetailsByOrderId(gsrOrderId);
//        for (ChequeDetails chequeDetailse : chequeDetailses) {
//            details.add(chequeDetailse);
//            chequeAmount += chequeDetailse.getAmount().doubleValue();
//        }
//        allPaymentDetails.setFull_cheque_amount(chequeAmount);
//        allPaymentDetails.setChequeReport(details);
//        paymentlist.add(allPaymentDetails);
//        return paymentlist;
//    }



}
