package org.egreen.opensms.server.model;

/**
 * Created by Pramoda Fernando on 3/23/2015.
 */
public class GsrPaymentModel {

    private Long gsrOrderId;
    private double totalAmount;
    private double paidAmount;
    private double remainingAmount;


    public Long getGsrOrderId() {
        return gsrOrderId;
    }

    public void setGsrOrderId(Long gsrOrderId) {
        this.gsrOrderId = gsrOrderId;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public double getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(double remainingAmount) {
        this.remainingAmount = remainingAmount;
    }
}
