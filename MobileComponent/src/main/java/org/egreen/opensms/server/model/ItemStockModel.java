package org.egreen.opensms.server.model;

/**
 * Created by dewmal on 12/16/14.
 */
public class ItemStockModel {

    private String itemId;
    private double maxQty;
    private String name;
    private String unitName;
    private String brandName;
    private double orderQty;
    private double preHasBatchPrice;


    public double getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(double orderQty) {
        this.orderQty = orderQty;
    }

    public double getPreHasBatchPrice() {
        return preHasBatchPrice;
    }

    public void setPreHasBatchPrice(double preHasBatchPrice) {
        this.preHasBatchPrice = preHasBatchPrice;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public double getMaxQty() {
        return maxQty;
    }

    public void setMaxQty(double maxQty) {
        this.maxQty = maxQty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
