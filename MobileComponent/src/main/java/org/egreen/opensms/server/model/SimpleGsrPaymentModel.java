package org.egreen.opensms.server.model;

import java.sql.Timestamp;

/**
 * Created by Pramoda Fernando on 3/25/2015.
 */
public class SimpleGsrPaymentModel {

    private Long orderId;
    private Timestamp orderDate;
    private double amount;
    private Integer paymentType;


    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Timestamp getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Timestamp orderDate) {
        this.orderDate = orderDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }
}
