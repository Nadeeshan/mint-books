package org.egreen.opensms.server.dao;

import org.egreen.opensms.server.entity.AuthenticationDetails;

/**
 * Created by Pramoda Fernando on 1/27/2015.
 */
public interface AuthenticationDetailsDAOController  extends DAOController<AuthenticationDetails,String> {
    Integer check_email_validity(String email);

    AuthenticationDetails searchUserByEmail(String email);

    AuthenticationDetails login(String email, String userPassword);

    AuthenticationDetails reset_password(String email);

    AuthenticationDetails varify_detail(String varification_code, String detail_id);

    AuthenticationDetails get_all_details_by_email(String email);
}
