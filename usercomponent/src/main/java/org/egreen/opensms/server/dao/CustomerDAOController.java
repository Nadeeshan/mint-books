package org.egreen.opensms.server.dao;


import org.egreen.opensms.server.entity.Customer;


import java.util.List;

/**
 * Created by dewmal on 7/17/14.
 */
public interface CustomerDAOController extends DAOController<Customer, Long> {


    /**
     * Search users by search query
     *
     * @param searchQuery
     * @return
     */
    List<Customer> findUsers(String searchQuery);


    List<Customer> getUserByCity(String city);

    List<Customer> getAllCustomerbyName(String custName);


    List<Customer> search_all_sortByCustomerName();

    List<Customer> search_all_sortByCustomerOrderValue();

    List<Customer> testing_search_all();

}
