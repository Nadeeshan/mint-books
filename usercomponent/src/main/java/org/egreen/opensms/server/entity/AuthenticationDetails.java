package org.egreen.opensms.server.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * Created by Pramoda Fernando on 1/27/2015.
 */


@Entity
@Table(name = "authentication")
@JsonIgnoreProperties
public class AuthenticationDetails implements EntityInterface <String>{

    private String details_id;
    private String user_name;
    private String email;
    private String password;


    @Id
    @Column(name = "details_id")
    public String getDetails_id() {
        return details_id;
    }

    public void setDetails_id(String details_id) {
        this.details_id = details_id;
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthenticationDetails that = (AuthenticationDetails) o;

        if (details_id != null ? !details_id.equals(that.details_id) : that.details_id != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (user_name != null ? !user_name.equals(that.user_name) : that.user_name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = details_id != null ? details_id.hashCode() : 0;
        result = 31 * result + (user_name != null ? user_name.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

    @Override
    @Transient
    public String getId() {
        return getDetails_id();
    }


    @Override
    public String toString() {
        return "AuthenticationDetails{" +
                "details_id='" + details_id + '\'' +
                ", user_name='" + user_name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
