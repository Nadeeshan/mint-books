package org.egreen.opensms.server.models;

import org.egreen.opensms.server.entity.Customer;

/**
* Created by Pramoda Fernando on 10/9/2014.
*/
public class UserDetailModel{

    private Customer userContactDetail;
    private double orderAmount;


    public Customer getUserContactDetail() {
        return userContactDetail;
    }

    public void setUserContactDetail(Customer userContactDetail) {
        this.userContactDetail = userContactDetail;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(double orderAmount) {
        this.orderAmount = orderAmount;
    }
}


