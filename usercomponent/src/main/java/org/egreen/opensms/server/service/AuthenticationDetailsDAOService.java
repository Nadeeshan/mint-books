package org.egreen.opensms.server.service;


import org.egreen.opensms.server.dao.AuthenticationDetailsDAOController;
import org.egreen.opensms.server.entity.AuthenticationDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Random;

/**
 * Created by Pramoda Fernando on 1/27/2015.
 */


@Service
public class AuthenticationDetailsDAOService {

    @Autowired
    private AuthenticationDetailsDAOController authenticationDetailsDAOController;


    @Autowired
    private AuthenticationController authenticationController;

    /**
     * Sign up User
     *
     * @param authenticationDetails
     * @return
     * @author Pramoda Nadeeshan Fernando
     * @version 1.0
     * @since 2015-02-12 4.26PM
     */
    public String signup(AuthenticationDetails authenticationDetails) {
        authenticationDetails.setDetails_id(getDetailsId(5));
        String password = authenticationController.getEncryptWord(authenticationDetails.getPassword());
        authenticationDetails.setPassword(password);
        String s = authenticationDetailsDAOController.create(authenticationDetails);
        return s;
    }

    /**
     * Check Email Validity
     *
     * @param email
     * @return
     * @author Pramoda Nadeeshan Fernando
     * @version 1.0
     * @since 2015-02-12 4.26PM
     */
    public Integer check_email_validity(String email) {
        System.out.println("EMAIL : " + email);
        Integer res = authenticationDetailsDAOController.check_email_validity(email);
        return res;
    }

    /**
     * Get Generated Details Id
     *
     * @param len
     * @return
     * @author Pramoda Nadeeshan Fernando
     * @version 1.0
     * @since 2015-02-12 4.26PM
     */
    private String getDetailsId(int len) {
        final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    /**
     * Get Genarated Voco Id
     *
     * @param len
     * @return
     * @author Pramoda Nadeeshan Fernando
     * @version 1.0
     * @since 2015-02-12 4.26PM
     */
    private String getVocoId(int len) {
        final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    /**
     * Genarated Attribute Id
     *
     * @param len
     * @return
     * @author Pramoda Nadeeshan Fernando
     * @version 1.0
     * @since 2015-02-12 4.26PM
     */
    private String getAttributeId(int len) {
        final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }


    /**
     * User Login
     *
     * @param email
     * @param password
     * @return
     * @author Pramoda Nadeeshan Fernando
     * @version 1.0
     * @since 2015-02-12 4.26PM
     */
    public AuthenticationDetails login(String email, String password) {
        String userPassword = authenticationController.getEncryptWord(password);
        System.out.println("User Pass : " + userPassword);
        AuthenticationDetails authenticationDetails = authenticationDetailsDAOController.searchUserByEmail(email);
        System.out.println(authenticationDetails);
        // System.out.println("DB Pass : "+user.getPassword());
        AuthenticationDetails returnUser = null;
        if (authenticationDetails != null) {
            if (userPassword.equals(authenticationDetails.getPassword())) {
                System.out.println("user");
                returnUser = authenticationDetailsDAOController.login(email, userPassword);
            } else {
                returnUser = null;
            }
        }
        return returnUser;
    }

    /**
     * Reset Password
     *
     * @param email
     * @return
     * @author Pramoda Nadeeshan Fernando
     * @version 1.0
     * @since 2015-02-12 4.26PM
     */
    public Integer reset_password(String email) {
        AuthenticationDetails authenticationDetails = authenticationDetailsDAOController.reset_password(email);
        if (authenticationDetails != null) {
            String new_password = randomString(5);

            String encryptWord = authenticationController.getEncryptWord(new_password);
            authenticationDetails.setPassword(encryptWord);
            authenticationDetailsDAOController.update(authenticationDetails);
            return 1;
        } else {
            return 0;
        }

    }


    /**
     * Get Random String
     *
     * @param len
     * @return
     * @author Pramoda Nadeeshan Fernando
     * @version 1.0
     * @since 2015-02-12 4.26PM
     */
    private String randomString(int len) {
        final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

//    /**
//     * Verify Detail
//     *
//     * @param varification_code
//     * @param detail_id
//     * @return
//     * @author Pramoda Nadeeshan Fernando
//     * @version 1.0
//     * @since 2015-02-12 4.26PM
//     */
//    public String varify_detail(String varification_code, String detail_id) {
//        AuthenticationDetails authenticationDetails = authenticationDetailsDAOController.varify_detail(varification_code, detail_id);
//        //  String encryptWord = authenticationController.getEncryptWord(authenticationDetails.getEmail());
//        String value;
//        if (varification_code.equals(authenticationDetails.getVoco_id())) {
//            value = "redirect:http://localhost:63342/VOCOLayout/index.html#/";
////            user.setIsvalid(true);
////            userDAOController.update(user)
//        } else {
//            value = "NOT VALID";
//        }
//        return value;
//    }

    /**
     * Get all Details By Detail Id
     *
     * @param detail_id
     * @return
     * @author Pramoda Nadeeshan Fernando
     * @version 1.0
     * @since 2015-02-12 4.26PM
     */
    public AuthenticationDetails get_all_details_byUserId(String detail_id) {
        return authenticationDetailsDAOController.read(detail_id);
    }


    /**
     * Get all details by Email
     *
     * @param email
     * @return
     * @author Pramoda Nadeeshan Fernando
     * @version 1.0
     * @since 2015-02-12 4.26PM
     */
    public AuthenticationDetails get_all_details_by_email(String email) {

        return authenticationDetailsDAOController.get_all_details_by_email(email);
    }


}
